===============================================================================
      README.TXT - SCHEDULER MULTIAGENT SYSTEM
===============================================================================

   Northwest Missouri State University
   
   Application based on the AASIS framework.
   
   Unless required by applicable law or agreed to in writing, software
   is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
   
   Will Hargrave
   44-499-05 Indpendent Study in Computer Science
   Spring 2016

   Denise Case, Assistant Professor
   dcase@nwmissouri.edu

===============================================================================
       ABOUT THIS PROJECT
===============================================================================
   This project provides intelligent agent-based scheduling assistance. 

   This project uses OMACS, OBAA++, and GMoDS and is based on the AASIS architecture.

   This project doesn't require MatLab or any external messaging support.

===============================================================================
      DEPENDENCIES - DEVELOPMENT
===============================================================================

Install JDK 1.8
   
  
===============================================================================
      Integrated Development Environment (IntelliJ or Eclipse)
===============================================================================
   
Install at least one IDE.

  I recommend IntellijIDEA 15 (or greater).
    http://www.jetbrains.com/idea/
    Intellij integrates with our other option, Eclipse.  
 
  Eclipse Kepler SR2: Standard 4.3.2 with Java 8 support
    http://www.eclipse.org/downloads/index-java8.php
    eclipse-standard-kepler-SR2-Java8-win32-x86_64.zip
    I extracted the contents to:
    C:\eclipse-standard-kepler-SR2-Java8-win32-x86_64
    Note: Java and Eclipse must be be either BOTH 32-bit OR BOTH 64-bit.
    The modified eclipse.ini file is included below. 

  Then through "new software":
    Java8 Support: 
    http://download.eclipse.org/eclipse/updates/4.3-P-builds/
	
The following Eclipse plugins are used for modifying goal & role 
models and for running Spock tests, respectively:
	
    AgentTool3 Eclipse plugin (Core and Process Editor) from
        http://agenttool.cis.ksu.edu/
        
   Groovy Eclipse plugin from 
        http://dist.springsource.org/release/GRECLIPSE/e4.3/

===============================================================================
      ECLIPSE WORKSPACE FOLDERS
=============================================================================== 
  Eclipse uses workspace folders to hold IDE configuration information that 
  we do not want shared across machines. Our current location is a 
  sub folder of the associated workspace. See:
  http://eclipse.dzone.com/articles/eclipse-workspace-tips
  for more information. 
        
===============================================================================
      SOURCE CODE
===============================================================================        

Install Git for source control.  

===============================================================================
       QUICK START
===============================================================================
	
Check out code.
	Go to a folder where you want to keep your code, say a "projects" folder.
	Use Git to clone the code from 
	https://w19950@bitbucket.org/w19950/basic-multi-agent-system.git

	Goto the scheduler folder and ppen a command window.  Type:
	gradlew run

Verify display.
	When everything is running successfully, you'll see screens appear showing
	agents and assigned tasks.

To create documentation, open a command window and run
    gradlew javaDoc


===============================================================================
     Gradle build tool (optional)
===============================================================================    
   
You can build the project with the gradle wrapper (included).

===============================================================================
       CONFIGURATION FILES
===============================================================================
		
The following configuration files are required:

	Agent.xml  	- local organization agents and their capabilities.
	Initialize.xml 	- provides custom goal parameters.
	GoalModel.goal 	- describes the objectives of the system
	RoleModel.role 	- describes capabilities needed to play roles and
                          roles that agents can play to achieve specific goals.
                          
The utils package has builder programs to assist with auto-generation of the 
many agent and initialize files used in the test cases.

===============================================================================
       EDITING CONFIGURATION FILES
===============================================================================

Two of the configuration files - the goal models (.goal) and the
role models (.role) can be easily edited using the recently updated
agentTool3 modeling plugins.

See http://agenttool.cis.ksu.edu for more information.


===============================================================================
       ADDING NEW BEHAVIOR
===============================================================================

Please reference the file titled "LearningAOSEandOMACSwithOBAA.docx" in the /ref directory for more information about this project as well as the process for adding new behavior to this system.


===============================================================================
  END
===============================================================================
