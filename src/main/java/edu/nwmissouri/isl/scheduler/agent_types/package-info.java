/**
 * Provides classes for custom agent types.
 */
package edu.nwmissouri.isl.scheduler.agent_types;