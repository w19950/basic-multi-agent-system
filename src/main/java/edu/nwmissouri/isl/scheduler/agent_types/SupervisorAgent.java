/**
 *
 * Copyright 2016 William Hargrave, Denise Case
 * Intelligent Systems Lab Northwest Missouri State University
 *
 * See License.txt file for the license agreement.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package edu.nwmissouri.isl.scheduler.agent_types;


import edu.ksu.cis.macr.obaa_pp.org.IExecutableOrganization;
import edu.nwmissouri.isl.scheduler.agent.BaseAgent;
import org.w3c.dom.Element;

/**
 * A supervisor agent runs the internal organization of agents associated organization-agent.
 */
public class SupervisorAgent extends BaseAgent {
    /**
     * Constructs a new instance in accordance with the provided information. Additional agent
     * capabilities can be specified in the agent configuration file (e.g. Agent.xml).
     *
     * @param name a string containing the unique name of this agent.
     */
    public SupervisorAgent(final String name) {
        super(name);
    }

    /**
     * Constructs a new instance in accordance with the provided information. Additional agent
     * capabilities can be specified in the agent configuration file (e.g. Agent.xml).
     *
     * @param organization the SelfOrganization organization, containing information about agents and objects in the
     *                     SelfOrganization system.
     * @param name         a string containing the unique name of this agent.
     * @param knowledge    an XML representation of the agents knowledge of the SelfOrganization and the organization.
     */
    public SupervisorAgent(final IExecutableOrganization organization, final String name, final Element knowledge) {
        super(organization, name, knowledge);
    }

    @Override
    public String toString() {
        return "SupervisorAgent{" +
                super.toString() +
                '}';
    }
}
