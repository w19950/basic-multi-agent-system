/**
 *
 * Copyright 2016 William Hargrave, Denise Case
 * Intelligent Systems Lab Northwest Missouri State University
 *
 * See License.txt file for the license agreement.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package edu.nwmissouri.isl.scheduler;

import ch.qos.logback.core.Context;
import ch.qos.logback.core.util.StatusPrinter;
import edu.ksu.cis.macr.obaa_pp.org.IExecutableOrganization;
import edu.ksu.cis.macr.obaa_pp.org.OrganizationFactory;
import edu.ksu.cis.macr.obaa_pp.player.Player;
import edu.ksu.cis.macr.obaa_pp.spec.IOrganizationSpecification;
import edu.ksu.cis.macr.obaa_pp.spec.OrganizationSpecification;
import edu.ksu.cis.macr.obaa_pp.views.OrganizationView;
import edu.nwmissouri.isl.scheduler.config.RunManager;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Starts up a group of organization-based agents.
 */
public enum BasicMultiAgentSystem {
    /**
     * Singleton instance of the Launcher (one per JVM).
     */
    INSTANCE;
    private static final Logger LOG = LoggerFactory.getLogger(BasicMultiAgentSystem.class);
    private static final boolean debug = true;
    private static boolean stepInLauncher = false;


    /**
     * Allows the external setting (either through GUI or config file to determine whether step mode should stop at steps in
     * this file.
     *
     * @return true if stepping in this file is desired; false if we should not stop in this file even during step
     * mode.
     */
    public static boolean isStepInLauncher() {
        return stepInLauncher;
    }

    /**
     * Sets whether to stop in this file during step mode.
     *
     * @param stepInLauncher - true if stepping in this file is desired; false if we should not stop in this file even during
     *                       step mode.
     */
    public static void setStepInLauncher(final boolean stepInLauncher) {
        BasicMultiAgentSystem.stepInLauncher = stepInLauncher;
    }

    /**
     * Prints out to the console the help for launching the main.
     *
     * @param argumentsDescription the description of the command-line arguments.
     */
    @SuppressWarnings("unused")
	private static void printHelp(final String[] argumentsDescription) {
        System.out.println(String.format("%d arguments required:",
                argumentsDescription.length));
        for (int i = 0; i < argumentsDescription.length; i++) {
            System.out.println(String.format("arg%d - %s", i,
                    argumentsDescription[i]));
        }
    }

    /**
     * The main program that runs a single executable organization of agents.
     *
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Thread.currentThread().setName("BasicMultiAgentSystem");

        
        // SCRIPT THE AGENT.XML file from the contents of INitialize.xml ******************************************************************
        //***************************************************************************************
        if (stepInLauncher) {
            Player.setStepMode(Player.StepMode.STEP_BY_STEP);
        }

        // assume SLF4J bound to logback in the current environment
        Context lc = (Context) LoggerFactory.getILoggerFactory();
        LOG.info("========= GOT LOGGER CONTEXT  =========\n\n");

        // print logback's internal status
        StatusPrinter.print(lc);
        LOG.info("========= DISPLAYED LOGBACK STATUS  =========\n\n");

        // delete any existing logfiles
        cleanLogFiles();
        LOG.info("========= DELETED LOG FILES  =========\n\n");

        // load user parameters for this run. Includes scenario, player information
        edu.ksu.cis.macr.obaa_pp.config.RunManager.load();
        LOG.info("========= READ OBAA++ USER INPUTS {}  =========\n\n");

        RunManager.load();
        LOG.info("========= READ APP USER INPUTS {}  =========\n\n");
        step();

        // get the organization specification folder
        final File folder = new File(RunManager.getAbsolutePathToTestCaseFolder());
        LOG.info("========= FOUND FOLDER {}  =========\n\n", folder);
        step();

        // get the organization specification
        IOrganizationSpecification spec;
        try {
            spec = new OrganizationSpecification(folder.getAbsolutePath());
            LOG.info("========= CREATED SPECIFICATION: {}  =========\n\n", spec);
        } catch (final Exception e) {
            LOG.error("ERROR: Could not initialize org {}. {}", folder, e.getMessage());
            return;
        }
        step();

        IExecutableOrganization org = null;
        try {
            org = OrganizationFactory.getOrganization(spec);
            LOG.info("org:{}", org);
            LOG.info("========= CREATED ORG {}  =========\n\n", folder.getName());
        } catch (final Exception e) {
            LOG.error("ERROR: Could not initialize org {}. {}", folder, e.getMessage());
            System.exit(-1);
        }
        step();

        // display org viewer (for the control component parts)
        OrganizationView.createOrganizationView(org);
        LOG.info("========= VIEW STARTED  =========\n\n");
        step();

        // run organization
        LOG.info("========= STARTING ORGANIZATION  =========\n\n");
       org.run();
    }

    private static void cleanLogFiles() {
        try {
            String curDir = System.getProperty("user.dir");
            FileUtils.cleanDirectory(new File(String.format("%s//logs", curDir)));
        } catch (Exception ex) {
            LOG.debug("Some logfiles in use. Cannot delete.");
        }
    }

    /**
     * Calls step. Allows shutting off the stepping just in the launcher file if stepInLauncher == false.
     */
    private static void step() {
        // change this as desired to test parts of the system
        if (stepInLauncher) {
            Player.step();
        }
    }

    @Override
    public String toString() {
        return "Launcher{" +
                "INSTANCE=" + INSTANCE +
                ", stepInLauncher=" + stepInLauncher +
                '}';
    }
}
