/** * Provides the control component master class referenced in agent.xml. */
package edu.nwmissouri.isl.scheduler.cc_m;