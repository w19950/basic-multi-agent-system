/**
 *
 * Copyright 2016 William Hargrave, Denise Case
 * Intelligent Systems Lab Northwest Missouri State University
 *
 * See License.txt file for the license agreement.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package edu.nwmissouri.isl.scheduler.guidelines;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.soap.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NodeList;

public class InitialGuidelines implements Serializable, IInitialGuidelines {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(InitialGuidelines.class);
	private static final boolean debug = false;

	/**
	 * Construct new new {@code}SchedulingGuidelines} with default values.
	 */
	public InitialGuidelines() {

	}

	
}


