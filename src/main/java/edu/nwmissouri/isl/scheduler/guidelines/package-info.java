/** * Provides guidelines the intelligent agents can use to manage behavior. */
package edu.nwmissouri.isl.scheduler.guidelines;