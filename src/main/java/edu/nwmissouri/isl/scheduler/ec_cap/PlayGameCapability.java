/**
 *
 * Copyright 2016 William Hargrave, Denise Case
 * Intelligent Systems Lab Northwest Missouri State University
 *
 * See License.txt file for the license agreement.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package edu.nwmissouri.isl.scheduler.ec_cap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.ksu.cis.macr.goal.model.InstanceParameters;
import edu.ksu.cis.macr.obaa_pp.ec.base.IExecutionComponent;
import edu.ksu.cis.macr.obaa_pp.ec_cap.AbstractOrganizationCapability;
import edu.ksu.cis.macr.obaa_pp.objects.IDisplayInformation;
import edu.ksu.cis.macr.obaa_pp.org.IExecutableOrganization;
import edu.ksu.cis.macr.organization.model.InstanceGoal;
import edu.ksu.cis.macr.organization.model.identifiers.StringIdentifier;
import edu.ksu.cis.macr.organization.model.identifiers.UniqueIdentifier;

/**
 * The {@code SelfControlCapability} provides the ability to act autonomously.
 * To startup, access central control systems, get authorizations and new
 * capabilities, and initiate communications with other agents.
 */
public class PlayGameCapability extends AbstractOrganizationCapability {
	private static final boolean debug = true;
	private static final Logger LOG = LoggerFactory.getLogger(PlayGameCapability.class);
	private static final ArrayList<int[]> PLAYER_MOVE_QUEUE = RefereeGameCapability.PLAYER_MOVE_QUEUE;

	/**
	 * Construct a new instance.
	 *
	 * @param owner
	 *            - the agent possessing this capability.
	 * @param org
	 *            - the immediate organization in which this agent operates.
	 */
	public PlayGameCapability(final IExecutionComponent owner, final IExecutableOrganization org) {
		super(PlayGameCapability.class, owner, org);
		this.owner = owner;
	}

	public synchronized void callForConfiguration() {
		// TODO Add ability "phone home" on startup
	}

	public synchronized UniqueIdentifier findParticipant(String participantCapability) {
		if (debug)
			LOG.debug("Entering findParticipant with {}.", participantCapability);
		Class cap = null;
		try {
			cap = Class.forName(participantCapability);
		} catch (ClassNotFoundException e) {
			LOG.error("ERROR: trying to find participant but participantCapability is null.");
			System.exit(-52);
		}
		UniqueIdentifier sub = null;

		String meNoSelf = owner.getUniqueIdentifier().toString().replace("self", "");
		final Collection<IExecutionComponent> allPersona = null;
		for (IExecutionComponent p : allPersona) {
			String name = p.getUniqueIdentifier().toString();
			if (debug)
				LOG.debug("This persona in my org has name : {}", name);
			if (name.contains(meNoSelf + "in")) {
				if (debug)
					LOG.debug("Possible sub: {}", name);
				if (debug)
					LOG.debug("sub cap={}", participantCapability);

				if (p.getCapability(cap) != null) {
					sub = p.getUniqueIdentifier();
					if (debug)
						LOG.debug("Found sub: {}", name);
				}
			}
		}
		return sub;
	}

	@Override
	public synchronized double getFailure() {
		return 0;
	}

	/**
	 * Get all parameters from this instance goal and use them to initialize the
	 * capability.
	 *
	 * @param instanceGoal
	 *            - this instance of the specification goal
	 */
	public synchronized void init(InstanceGoal<?> instanceGoal) {
		LOG.info("Initializing capability from goal: {}.", instanceGoal);
		// Get the parameter values from the existing active instance goal
		final InstanceParameters params = Objects.requireNonNull((InstanceParameters) instanceGoal.getParameter());
		System.out.println(params.getParameters().get(StringIdentifier.getIdentifier("messageQueue")));
		if (debug)
			LOG.debug("Initializing params: {}.", params);
	}

	public void makeMove(int[][] board) {
		if (RefereeGameCapability.gameFinished)
			return;

		boolean player1 = getOwner().getUniqueIdentifier().toString().equals("Player 1");
		if (player1 ^ RefereeGameCapability.player1Move)
			return;

		ArrayList<int[]> openMoves = new ArrayList();
		for (int i = 0; i < board.length; i++)
			for (int j = 0; j < board[i].length; j++)
				if (board[i][j] == -1)
					openMoves.add(new int[] { i, j });

		Random r = new Random();
		int[] move = openMoves.get(r.nextInt(openMoves.size()));
		PLAYER_MOVE_QUEUE.add(new int[] { player1 ? 0 : 1, move[0], move[1] });
	}

	@Override
	public synchronized void populateCapabilitiesOfDisplayObject(final IDisplayInformation displayInformation) {
		super.populateCapabilitiesOfDisplayObject(displayInformation);
	}

	@Override
	public synchronized void reset() {
	}

	@Override
	public synchronized Element toElement(final Document document) {
		final Element capability = super.toElement(document);
		return capability;
	}

	/**
	 * Trigger the associated goal.
	 *
	 * @param instanceGoal
	 *            - the instance goal that is triggering the new goal.
	 */
	public synchronized void triggerGoals(final InstanceGoal<?> instanceGoal) {
		// nothing
	}
}
