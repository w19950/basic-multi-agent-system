/** * Provides the control component participant class referenced in agent.xml. */
package edu.nwmissouri.isl.scheduler.cc_p;