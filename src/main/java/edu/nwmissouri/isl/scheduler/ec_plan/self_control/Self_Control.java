/**
 *
 * Copyright 2012-2016 Denise Case Northwest Missouri State University
 *
 * See License.txt file for the license agreement.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package edu.nwmissouri.isl.scheduler.ec_plan.self_control;


import edu.ksu.cis.macr.obaa_pp.ec.base.IExecutor;
import edu.ksu.cis.macr.obaa_pp.ec.plans.IExecutablePlan;
import edu.ksu.cis.macr.obaa_pp.ec.plans.IPlanState;
import edu.ksu.cis.macr.organization.model.InstanceGoal;
import edu.nwmissouri.isl.scheduler.ec_cap.DateTimeCapability;
import edu.nwmissouri.isl.scheduler.ec_cap.SelfControlCapability;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * The {@code Self_Control} state is the main step in the {@code Self_Control_Plan}.  It will monitor connections and
 * attempt to restart any that have been dropped.  It retrieves messages from sensor sub agents and forwards them to sub
 * agents participating in external organizations, reviewing and biasing content before sending to reflect the multiple
 * objectives and biases of this agent.
 */
public enum Self_Control implements IPlanState<Self_Control_Plan> {
    INSTANCE;


    private static final Logger LOG = LoggerFactory.getLogger(Self_Control.class);
    private static final boolean debug = false;


    @Override
    public synchronized void Enter(final IExecutablePlan plan, final IExecutor ec, final InstanceGoal<?> ig) {
        // Nothing
    }

    @Override
    public synchronized void Execute(final IExecutablePlan plan, final IExecutor ec, final InstanceGoal<?> ig) {
        if (debug) LOG.debug("Starting with instance goal: {}.", ig);
        Objects.requireNonNull(ec);
        Objects.requireNonNull(ig);
        Objects.requireNonNull(ec.getCapability(SelfControlCapability.class), "Role requires SelfControlCapability.");
        Objects.requireNonNull(ec.getCapability(DateTimeCapability.class), "Role requires DateTimeCapability.");

        plan.heartBeat(this.getClass().getName());

        // initialize guidelines from instance goal
        ec.getCapability(SelfControlCapability.class).init(ig);
        if (debug) LOG.debug("Set SelfControlCapability guidelines from instance goal. {}", ig);

        String myPersona = ec.getUniqueIdentifier().toString();

        // get current timeSlice
        long currentTimeSlice = Objects.requireNonNull(ec.getCapability(DateTimeCapability.class).
                getTimeSlicesElapsedSinceStart(), "ERROR: Need a timeSlice to get sensor data.");
    }

    @Override
    public synchronized void Exit(final IExecutablePlan plan, final IExecutor ec, final InstanceGoal<?> ig) {
        // Nothing
    }
}
