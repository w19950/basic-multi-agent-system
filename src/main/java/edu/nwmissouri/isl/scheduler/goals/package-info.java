/** * Provides classes to specify the goal model for an organization. */
package edu.nwmissouri.isl.scheduler.goals;